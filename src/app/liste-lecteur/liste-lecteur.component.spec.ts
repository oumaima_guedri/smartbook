import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeLecteurComponent } from './liste-lecteur.component';

describe('ListeLecteurComponent', () => {
  let component: ListeLecteurComponent;
  let fixture: ComponentFixture<ListeLecteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeLecteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeLecteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
