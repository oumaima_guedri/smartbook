import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListeLecteurComponent } from './liste-lecteur/liste-lecteur.component';
import { ListeLibraireComponent } from './liste-libraire/liste-libraire.component';
import { InscritComponent } from './inscrit/inscrit.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeLecteurComponent,
    ListeLibraireComponent,
    InscritComponent,
    LoginComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
