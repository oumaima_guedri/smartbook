import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeLibraireComponent } from './liste-libraire.component';

describe('ListeLibraireComponent', () => {
  let component: ListeLibraireComponent;
  let fixture: ComponentFixture<ListeLibraireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeLibraireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeLibraireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
