import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InscritComponent } from './inscrit/inscrit.component';
import { ListeLecteurComponent } from './liste-lecteur/liste-lecteur.component';
import { ListeLibraireComponent } from './liste-libraire/liste-libraire.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path:'liste-lecteur',component:ListeLecteurComponent},
  {path:'liste-libraire',component:ListeLibraireComponent},
  {path:'login',component:LoginComponent},
  {path:'inscrit',component:InscritComponent},

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
